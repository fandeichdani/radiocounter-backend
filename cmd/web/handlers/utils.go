package handlers

import (
	"encoding/json"
	"log"
	"net/http"
)

func Message(status bool, message interface{}) map[string]interface{} {
	return map[string]interface{}{"status": status, "message": message}
}

func Respond(w http.ResponseWriter, data map[string]interface{}) {
	w.Header().Add("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(data); err != nil {
		log.Print(err)
	}
}
