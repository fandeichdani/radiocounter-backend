package handlers

import (
	"context"
	"github.com/gorilla/mux"
)

func Router(ctx context.Context) *mux.Router {
	r := mux.NewRouter()

	r.HandleFunc("/api/get", Get(ctx)).Methods("POST")
	r.HandleFunc("/healthz", healthz)
	r.HandleFunc("/livenez", livenez())
	r.HandleFunc("/readyz", readyz(ctx))
	return r
}
