package handlers

import (
	"context"
	"encoding/json"
	"log"
	"math"
	"math/rand"
	"net/http"
	"radio_counter/cmd/web/src"
	"time"
)

type jsonPost struct {
	Start string `json:"start"`
	Mod   string `json:"mod"`
}

type periodTime struct {
	Start time.Time
	End   time.Time
	Mod   string
}

type apiMsg struct {
	Status string            `json:"status,omitempty"`
	Data   map[string]uint16 `json:"data"`
}

type data struct {
	count int
	time  time.Time
}

func Get(ctx context.Context) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		periodJson := &jsonPost{}
		if err := json.NewDecoder(r.Body).Decode(&periodJson); err != nil {
			Respond(w, Message(false, "Invalid request"))
			return
		}
		period := periodTime{}
		timeStart, err := time.Parse("2006-01-02", periodJson.Start)
		if err != nil {
			Respond(w, Message(false, "Invalid request(start)"))
			return
		} else {
			period.Start = timeStart
			period.Mod = periodJson.Mod
		}

		switch period.Mod {
		case "day":
			period.End = period.Start.Add(time.Hour * 23)
		case "week":
			if period.Start.Weekday() != time.Monday {
				Respond(w, Message(false, "Invalid body of request(week)"))
				return
			}
			period.End = period.Start.AddDate(0, 0, 6).Add(time.Hour * 23)
		case "month":
			if period.Start.Day() != 1 {
				Respond(w, Message(false, "Invalid body of request(month)"))
				return
			}
			period.End = period.Start.AddDate(0, 1, 0).Add(-1 * time.Hour)
		case "year":
			if period.Start.Day() != 1 && period.Start.Month() != 1 {
				Respond(w, Message(false, "Invalid body of request(year)"))
				return
			}
			period.End = period.Start.AddDate(1, 0, 0).Add(-1 * time.Hour)
		default:
			Respond(w, Message(false, "Invalid mod of request"))
			return
		}
		db, ok := src.DBFromContext(ctx)
		if !ok {
			err := db.Close()
			if err != nil {
				log.Println(err)
				return
			}
			*db = *src.ConnectDB()
			Respond(w, Message(false, "DB unavailable"))
			return
		}
		currTime := period.Start
		msg := apiMsg{"OK", make(map[string]uint16)}
		rows, err := db.Query(`
	SELECT measure_dttm, sum(listeners_count) FROM radiocounter.measurements_1 WHERE measure_dttm BETWEEN ? AND ? group by measure_dttm order by measure_dttm
		`,
			period.Start.Format("2006-01-02 15:04:05"), period.End.Format("2006-01-02 15:04:05"))

		if err != nil {
			Respond(w, Message(false, "DB unavailable"))
			return
		}
		map_data := make(map[string]uint16)
		for rows.Next() {
			var Data data
			if err = rows.Scan(&Data.time, &Data.count); err != nil {
				log.Print(err)
			} else {
				map_data[Data.time.Format("2006-01-02 15:04:05")] = uint16(float64(Data.count) * 1.7)
			}
		}
		err = rows.Close()
		if err != nil {
			log.Print(err)
		}
		for currTime = period.Start; currTime != period.End.Add(1*time.Hour); currTime = currTime.AddDate(0, 0, 1) {
			pred := data{-1337, time.Now()}
			rand.Seed(1337)
			for hourTime := currTime; hourTime != currTime.AddDate(0, 0, 1); hourTime = hourTime.Add(1 * time.Hour) {
				if pred.count == -1337 {
					count, err := map_data[hourTime.Format("2006-01-02 15:04:05")]
					if err == true {
						pred = data{int(count), hourTime}
					} else {
						map_data[hourTime.Format("2006-01-02 15:04:05")] = uint16(0)
					}
				} else {
					count, err := map_data[hourTime.Format("2006-01-02 15:04:05")]
					if err == true {
						diff := int(math.Abs(float64(count) - float64(pred.count)))
						if diff == 0 {
							diff = int(count/10) + 10
						}
						pred.time = pred.time.Add(1 * time.Hour)
						for ; pred.time != hourTime; pred.time = pred.time.Add(1 * time.Hour) {
							map_data[pred.time.Format("2006-01-02 15:04:05")] = uint16(math.Min(float64(count), float64(pred.count))) + uint16(rand.Intn(diff))
						}
						pred = data{int(count), hourTime}
					} else {
						map_data[hourTime.Format("2006-01-02 15:04:05")] = uint16(0)
					}
				}
			}
		}

		switch period.Mod {
		case "day":
			msg.Data = map_data
		case "week":
			for currTime = period.Start; currTime != period.End.Add(1*time.Hour); currTime = currTime.AddDate(0, 0, 1) {
				Count := uint16(0)
				for dayTime := currTime; dayTime != currTime.AddDate(0, 0, 1); dayTime = dayTime.Add(time.Hour * 1) {
					tmp := map_data[dayTime.Format("2006-01-02 15:04:05")]
					Count += tmp
				}
				msg.Data[currTime.Format("2006-01-02")] = Count
			}
		case "month":
			for currTime = period.Start; currTime != period.End.Add(1*time.Hour); currTime = currTime.AddDate(0, 0, 1) {
				Count := uint16(0)
				for dayTime := currTime; dayTime != currTime.AddDate(0, 0, 1); dayTime = dayTime.Add(time.Hour * 1) {
					tmp := map_data[dayTime.Format("2006-01-02 15:04:05")]
					Count += tmp
				}
				msg.Data[currTime.Format("2006-01-02")] = Count
			}
		case "year":
			for currTime = period.Start; currTime != period.End.Add(1*time.Hour); currTime = currTime.AddDate(0, 1, 0) {
				Count := uint16(0)
				for monthTime := currTime; monthTime != currTime.AddDate(0, 1, 0); monthTime = monthTime.Add(time.Hour * 1) {
					tmp := map_data[monthTime.Format("2006-01-02 15:04:05")]
					Count += tmp
				}
				msg.Data[currTime.Format("2006-01")] = Count
			}
		}
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		err = json.NewEncoder(w).Encode(msg)
		if err != nil {
			log.Print(err)
		}
		w.Header().Add("Access-Control-Allow-Origin", "*")
	}
}
