package handlers

import (
	"encoding/json"
	"log"
	"net/http"
)

func livenez() http.HandlerFunc {
	return func(w http.ResponseWriter, _ *http.Request) {
		ready := make(map[string]string)
		ready["status"] = "ok"
		w.Header().Set("Content-Type", "application/json")
		js, err := json.Marshal(ready)
		if err != nil {
			log.Println("err")
		}
		_, err = w.Write(js)
		if err != nil {
			log.Print(err)
		}
		log.Print("live probe positive")
	}
}
