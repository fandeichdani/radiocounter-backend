package handlers

import (
	"context"
	"log"
	"net/http"
	"radio_counter/cmd/web/src"
	"time"
)

func readyz(ctx context.Context) http.HandlerFunc {
	return func(w http.ResponseWriter, _ *http.Request) {
		db, ok := src.DBFromContext(ctx)
		if !ok {
			w.WriteHeader(http.StatusForbidden)
			Respond(w, Message(false, "db cann't get"))
			log.Print("ready probe negative")
			return
		}
		ctxTime, cancel := context.WithTimeout(ctx, 500*time.Millisecond)
		defer cancel()
		clickHousePingResult := make(chan error)
		go func() {
			clickHousePingResult <- db.Ping()
		}()
		select {
		case <-ctxTime.Done():
			w.WriteHeader(http.StatusForbidden)
			Respond(w, Message(false, "db cann't connect"))
			log.Print("ready probe negative")
		case <-clickHousePingResult:
			var version string
			err := db.QueryRow("SELECT VERSION()").Scan(&version)
			if err != nil {
				Respond(w, Message(false, "cann't select version"))
				log.Print("ready probe negative")
			} else {
				Respond(w, Message(true, version))
				log.Print("live probe positive")
			}
		}
	}
}
