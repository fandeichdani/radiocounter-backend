package main

import (
	"context"
	"github.com/rs/cors"
	"log"
	"net/http"
	"os"
	"radio_counter/cmd/web/handlers"
	"radio_counter/cmd/web/src"
)

func main() {
	ctx := context.WithValue(context.Background(), "db", src.ConnectDB())

	log.Print("Starting...")
	port := os.Getenv("PORT")
	if port == "" {
		port = "5000"
	}
	r := handlers.Router(ctx)
	srv := &http.Server{
		Addr:    ":" + port,
		Handler: cors.Default().Handler(r),
	}
	go src.Home(ctx)
	log.Print("The service is ready to listen and serve.")
	log.Fatal(srv.ListenAndServe())
}
