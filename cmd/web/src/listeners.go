package src

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"
	"time"
)

const (
	RadioHeart = "https://a4.radioheart.ru/api/json?userlogin=user8042&api=current_listeners"
	MyRadio24  = "http://myradio24.com/users/meganight/status.json"
)

func GetListenersRadioHeart() (int, error) {
	url := RadioHeart
	var netClient = &http.Client{
		Timeout: time.Second * 1,
	}

	req, _ := http.NewRequest("GET", url, nil)
	res, err := netClient.Do(req)
	if err != nil {
		return -1, err
	}
	var result map[string]string
	err = json.NewDecoder(res.Body).Decode(&result)
	if err != nil {
		return 0, err
	}
	num, Err := result["listeners"]
	if Err == false {
		return 0, errors.New("empty result_map")
	}
	out, err := strconv.Atoi(num)
	if err != nil {
		return 0, err
	}
	return out, nil
}

func GetListenersMyRadio24() (int, error) {
	url := MyRadio24
	var netClient = &http.Client{
		Timeout: time.Second * 1,
	}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return 0, err
	}
	res, err := netClient.Do(req)
	if err != nil {
		return 0, err
	}

	var result map[string]interface{}
	err = json.NewDecoder(res.Body).Decode(&result)
	if err != nil {
		return 0, err
	}
	num, ok := result["listeners"]
	if ok == false {
		return 0, errors.New("empty result_map")
	}
	numFloat, ok := num.(float64)
	if !ok {
		return 0, errors.New("cann't convert")
	}
	return int(numFloat), nil
}
