package src

import (
	"context"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/github"
	_ "github.com/jackc/pgx/stdlib"
	"time"
)

func Home(ctx context.Context) {
	var insert = make(chan DbType)
	go StartDB(ctx, &insert)
	count1, _ := GetListenersMyRadio24()
	count2, _ := GetListenersRadioHeart()
	var (
		count11, count22 int
		err              error
	)
	for {
		TimeNow := time.Date(time.Now().Year(), time.Now().Month(), time.Now().Day(), time.Now().Hour(), 0, 0, 0, time.UTC)
		count11, err = GetListenersMyRadio24()
		if count11-count1 > 0 && err == nil {
			insert <- DbType{TimeNow, uint16(count11 - count1)}
		}
		if count11 > 0 {
			count1 = count11
		}

		count22, err = GetListenersRadioHeart()
		if count22-count2 > 0 && err == nil {
			insert <- DbType{TimeNow, uint16(count22 - count2)}
		}
		if count22 > 0 {
			count2 = count22
		}
	}

}
