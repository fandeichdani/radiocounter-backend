package src

import (
	"context"
	"database/sql"
	_ "github.com/ClickHouse/clickhouse-go"
	"log"
	"os"
	"time"
)

type DbType struct {
	Time  time.Time
	Count uint16
}

func ConnectDB() *sql.DB {
	dsn := os.Getenv("DATABASE_URL")
	db, err := sql.Open("clickhouse", dsn) // *sql.DB
	if err != nil {
		log.Printf("failed to load driver: %v", err)
	}
	return db
}

func DBFromContext(ctx context.Context) (*sql.DB, bool) {
	u, ok := ctx.Value("db").(*sql.DB) //Я уверен что будет лежать база данных, поэтому так делаю
	return u, ok
}

func StartDB(ctx context.Context, insert *chan (DbType)) {
	db, ok := DBFromContext(ctx)
	if !ok {
		log.Println(ok)
	}
	for {
		select {
		case elem := <-*insert:
			err := insertDB(db, elem)
			for err != nil {
				if db.Ping() != nil {
					err = db.Close()
					if err != nil {
						log.Print(err)
					}
					*db = *ConnectDB()
				}
				err = insertDB(db, elem)
			}
		}
	}
}

func insertDB(db *sql.DB, elem DbType) (err error) {
	tx, err := db.Begin()
	if err != nil {
		return
	}
	stmt, err := tx.Prepare("INSERT INTO radiocounter.measurements_1 VALUES (toDateTime(?, 'Europe/Moscow'), ?)")
	if err != nil {
		return
	}
	defer func() {
		err := stmt.Close()
		if err != nil {
			log.Println(err)
		}
	}()
	_, err = stmt.Exec(
		elem.Time,
		elem.Count,
	)
	if err := tx.Commit(); err != nil {
		log.Print(err)
	}
	moscow, _ := time.LoadLocation("Europe/Moscow")
	log.Print(elem.Time.In(moscow).Format("2006-01-02 15:04:05"), " : ", elem.Count)
	return
}
