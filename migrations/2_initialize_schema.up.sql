CREATE TABLE radiocounter.measurements_1
(
    measure_dttm    DateTime,
    listeners_count UInt16
) ENGINE = SummingMergeTree()
      ORDER BY measure_dttm;
drop table radiocounter.measurements;